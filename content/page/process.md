---
title: I am not a game theorist ...
subtitle: ... but I play one on TV!
comments: false
---


Whore's Radish is NOT a zero-sum game like gambling casinos or cryptocurrencies exchanges are ... a [zero-sum game](https://en.wikipedia.org/wiki/Zero-sum_game) ... because the the Whore's Radish game is about exchanging plant materials, cuttings of different plants and information about plants ... learning to build soil, propogate and care for plants and then to use plants for improving the home environment, seasoning food or even for medicinal uses. 

In other words, the Whore's Radish is about growth, it's an ***EXPANDING sum*** game in many dimensions ... when you think of the Whore's Radish, visualize ***EXPANDING*** your own sum of friends and people who share your plant passion and motivation ... as a game, the Whore's Radish is all about engagement, enjoying the community and being part of something that more than the sum of its parts.