---
title: Alphabetic Index of Plant Lists
subtitle: Curated, Annotated Lists
comments: false
---


- [A](#a)
- [B](#b)
- [C](#c)
- [D](#d)
- [E](#e)
- [F](#f)
- [G](#g)
- [H](#h)
- [I](#i)
- [J](#j)
- [K](#k)
- [L](#l)
- [M](#m)
- [O](#o)
- [P](#p)
- [Q](#q)
- [R](#r)
- [S](#s)
- [T](#t)
- [U](#u)
- [V](#v)
- [W](#w)
- [Xa](#xa)
- [Y](#y)
- [Z](#z)

## A

## B

* [Begonia](begonia)

## C

* [Cannabis](cannabis)

* [Coco](coco)

* [Coffee](coffee)

## D

## E

## F

## G

* [Geranium](geranium)

## H

## I

## J

## K

## L

## M

## O

## P

* [Poppies](poppies)

## Q

## R

* [Rose](rose)
## S

## T

## U

## V

## W

WTF
## Xa

*Yeah, there are [names of plants start with the letter "Xa" or, for that matter, "Xe" or 'Xy"](https://mygardenflowers.com/flower-names/x/) ... but Markdown robo-processors think they have first claim on [X] ... so we start with "Xa" *

* [Xanthoceras Sorbifolium](xanthocerassorbifolium)
* [Xeranthemum](xeranthemum)
* [Xerophyllum](xerophyllum)
* [Xylobium](ylobium)
* [Xylosma](xylosma)
* [Xyris Difformis](xyrisdifformis)
* [Xanthisma](xanthisma)
* [Xerochrysum](xerochrysum)
* [Xyris](xyris)

## Y

## Z
