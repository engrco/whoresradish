---
title: AWESOME Begonias
subtitle: If you don't like the Whore's Radish, fork it!
tags: ["bigimg", "salebarn"]
bigimg: [{src: "https://petesgreenhouse.com/wp-content/uploads/sites/12904/2020/06/839A2793-1-1536x1024.jpg", desc: "Care Indoors"}, {src: "https://gardeningsolutions.ifas.ufl.edu/images/plants/flowers/begonia_variegated.jpg", desc: "Propagation Methods"}, {src: "https://i.pinimg.com/564x/fc/d3/e8/fcd3e8861b050c8e3158a99cabf53f74.jpg", desc: "Varieties, Cultivars, Species"}, {src: "https://i0.wp.com/ahealthylifeforme.com/wp-content/uploads/2013/08/Propagating-Begonias.jpg", desc: "Breeding of Begonias"}, {src: "https://files.greenhousegrower.com/greenhousegrow/wp-content/uploads/2018/10/IMG_8133.jpg", desc: "Research And Biotechnology"}]
---


- [Introduction](#introduction)
- [Care](#care)
- [Propagation](#propagation)
- [Varieties](#varities)
- [Breeding](#breeding)
- [Research](#research)


The AWESOME lists on Whore's Radish represent a riff on the game [*Cadavre Exquis*](https://en.wikipedia.org/wiki/Exquisite_corpse) ... except, this collection is more about the process of exchanging AWESOME plant information and stories ... so it's a story completion game, in which ***your experience***, real or realistic enough to be equivalent to real, completes the story.

Well, okay ... it's about information and stories, not another boring plant exchange game ... or a drinking game ... or even offbeat tangy wordplay ... but it can be ALL of those ... and MORE!


## Introduction

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*

## Care

In matters like this, keep your grow VERY simple... [just like growing begonias](http://www.begonias.org/care/culture.htm) ... the principles behind caring for begonias are not that much different from many other plants ... just be extremely sure that you have the BASICS down.

* The soil mix should be ever so slightly acidic, very well-drained with very high amount of well-decomposed, mostly digested organic matter like peat moss [which is now decomposing moss which was growing on decomposed fungi that have digested the wood in the peat].

* Do NOT overwater or continually add water, but water thoroughly like a soaking rain when the top becomes dry to the touch. It does not hurt to allow the plant to wilt ... if you observe the condition and notice things, ie why do you have plants if you don't want to do this?

* As a general rule, go way easier on the fertilizer than you might think -- use at most 1/4 or less of the recommended amount of fertilizer, but apply the smaller amount more often than recommended. In general, remember that plants do not have stomachs, intestinal tracts, eat meals or store fat like animals, they absorb nutrients over a long period of time, not in convenient feedings or annual applications as you might find in field crops [which were bred/selected for their ability to use fertility in large dollops].

* Don't prune through the winter, but wait until early spring when new growth starts due to added sunlight. If you are not specifically aiming to propagate using wild overgrowth, soft pinch growing tips throughout the growing season to shape plants, fill them out, and keep them within bounds ... begonias are not pruned like fruit trees, ie pruned/shaped in the winter.

* Begonias have shallow root systems and prefer shallow, smallish pots. Begonias do not respond favorably to overpotting -- so let them crowd the pot and demand to be divided or pruned ... much of the fun of begonias is in division of the plants and in propogations using the brandes of healthy overgrown plants.

## Propagation

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*

## Varieties

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*

## Breeding

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*

## Research

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*