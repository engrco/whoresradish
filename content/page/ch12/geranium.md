---
title: AWESOME Coffee
subtitle: If you don't like the Whore's Radish, fork it!
comments: false
---


The AWESOME lists on Whore's Radish represent a riff on the game [*Cadavre Exquis*](https://en.wikipedia.org/wiki/Exquisite_corpse) ... except, this collection is more about the process of exchanging AWESOME plant info ... 

Well, okay ... this doesn't have to be another boring plant exchange game ... or a drinking game ... or even offbeat tangy wordplay ... it can be ALL of those ... and MORE!

And if you don't like this game of Whore's Radish, improve it ... or, WTH ... *just fork it!*