---
title: AWESOME Coco
subtitle: 
comments: false
---

You can probably do much better, so fork [this list](https://gitlab.com/engrco/whoresradish/-/blob/master/content/page/ch12/coco.md)!

The place to start with every plant family is [the Wikipedia entry](https://en.wikipedia.org/wiki/Erythroxylaceae) or, if you don't know the family, start with the Wikipedia entry for the species you are most interested in such as perhaps the [Coco plant species, especially cultivated in South America](https://en.wikipedia.org/wiki/Coca) to yield the psychoactive alkaloid, cocaine.

Obviously, growing something like coco is going to present some challenges ... but that has perhaps been part of the allure of growing things like [cannabis](https://engrco.gitlab.io/whoresradish/page/ch12/cannabis/) or [poppies](https://engrco.gitlab.io/whoresradish/page/ch12/poppies/) ... except that in the case of coco, there are extra difficulties for vendors in obtaining and selling the seeds, so starting seeds is going to be expensive and the selection of vendors is perhaps dicier than what one finds in cannabis or houseplants, although if this is something that you just need to do, you can [buy South American seeds online](https://www.bcseeds.com/product-category/coca-seeds/) and setting up the correct microclimate is a bit of a chore. For example, the optimum average daily temperature for coca plant growth is 27 C / 80.6 F; the optimum pH for growing coca plants from coca seeds is acidic at 4.7 - 6.0; and, if you can somehow get the seeds to sprout and seeds to grow, the first harvest of coca leaves takes place about 1 year after planting and there are not going to be that many leaves, if only because if you have healthy plants, you are going to want to micro-propagate and expand your coco forest.

Pictures have ways of leading to useful captions for investigating, so if you ever use social media, you might be tempted to scroll through things like the [#CocoPlant hashtag on Instagram](https://www.instagram.com/explore/tags/cocoplant/) ... but you will see very few if any pictures the [Coco plant species, of the kind cultivated in South America](https://en.wikipedia.org/wiki/Coca) ... so these pictures are going to be almost all pointless rabbithole scenery ... when it comes to the Coco plant, one ***really*** needs to ask if the view is really worth the climb.

