---
title: The WHY of Whore's Radish
subtitle: Connecting the Connectors, the complementing META-community of communities
date: 2021-09-07
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

***What drives or gives us the impulse to say something kind or ever establish something akin to an organic, living connection?***

THAT is the question that provoked the creation of Whore's Radish ... and it's also about the fundamental WHY for WHY Whore's Radish exists.

Whore's Radish is like horseradish ... people do love the tangy taste of it ... but it generally used to complement the main act, not to completely overpower and steal the show.